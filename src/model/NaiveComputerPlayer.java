package model;

import cardgame.Strategy;
import controller.Game;
import model.KitCard;
import model.Player;

public class NaiveComputerPlayer extends Player {
    private Strategy strategy;
    public NaiveComputerPlayer(String playerName, Strategy strategy) {
        super(playerName);
        this.strategy = strategy;
    }

    public Strategy getStrategy() {
        return this.strategy;
    }

    public void playTheGame(Game game){

        while (true){
            int turn = strategy.determineMove(game, this);
            playCard(turn, game);
            break;
        }
        playCard(-1, game);


    }

    public void playCard(int whichone,Game game){
        if (whichone >= 0 && whichone < this.getDrawOfCards().size()) {
            KitCard cardToPlay = this.getDrawOfCards().get(whichone);

        }

    }
}
