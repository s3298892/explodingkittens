package model;

import model.CardDeck;
import model.KitCard;

import java.util.ArrayList;
import java.util.List;

/**
 * Player class
 * It is the representation of one of the players in a game of Exploding Kittens
 */
public class Player {

    public String playerName;
    public List<KitCard> drawOfCards;
    private boolean eliminatedPlayer = false;
    private int numbweOfPlayerTurns;
    private final int id;

    //constructor player

    /**
     * Pkayer constructor
     *
     * It makes a new player, who has a specific name
     *
     * @param playerName representing the name of the new player
     */
    public Player(String playerName, int id){
        this.id = id;
        this.playerName=playerName;
        drawOfCards = new ArrayList<>();
        this.numbweOfPlayerTurns = 1;
    }

    public int getId() {
        return this.id;
    }

    /**
     * Getter for the number of turns
     *
     * @return the number of turns a player must take
     */
    public int getNrOfTurns(){
        return this.numbweOfPlayerTurns;
    }

    /**
     * Method that increases the number of turns a player must take before ending his turn
     * It is reseted in case of an attack.
     *
     */
    public void increaseTurns() {
        this.numbweOfPlayerTurns += 1;
    }

    /**
     * Method that has the role of resetting each player tur n to one by the end of
     * his turn, so the default, without having any attacks thrown at him would always be 1
     */
    public void resetTurns() {
        this.numbweOfPlayerTurns = 1;
    }

    /**
     * Getter for the name of the player
     * @return the name of the player
     */
    public String getPlayerName() {
        return playerName;
    }

    /**
     * Method that facilitates the player to draw a card from the deck of cards
     *
     * @param deck represents the deck of cards from which the player is able to draw cards
     *             during the round that is being played
     * @requires deck not empty
     */
    public void drawCard(CardDeck deck){
        if (!deck.isEmpty()){
            KitCard newCard = deck.drawNewCard();
            drawOfCards.add(newCard);
        }

    }

    /**
     * Method that has the function of decreasing the number of turns a player must play
     * An essential method, as it bioth helps tracking when a turns must end, but also decrease down to zero
     * when a player is attacked and must take more than one turn
     *
     * @requires that numberOfPlayerTurns > 0 , as 0 means that the turn has ended and there is nothing you can decrease
     *            anymore
     */
    public void decreaseTurns() {
        if (this.numbweOfPlayerTurns > 0) {
            this.numbweOfPlayerTurns--;
        }
    }

    /**
     *
     * @return the status of the player within the game, true if it is eliminated, false if he is
     * still in the game
     */
    public boolean isEliminatedPlayer(){
        return this.eliminatedPlayer;


    }

    /**
     *
     * @return the hand of the player as a list of cads of type KitCard
     */
    public List<KitCard> getDrawOfCards() {
        return drawOfCards;
    }

    /**
     * Method that has the function to eliminate the player from the game
     * It is called in a game, when the player draws a card of type EXPLODING_KITTEN
     * and does not have a card of type DIFUSE in his hand
     */
    public void eliminate(){
        this.eliminatedPlayer = true;
        System.out.println(playerName);
    }

    /**
     * Sets the hand of the player, used at the beginning of a game for the initial hand
     * @param drawOfCards represents the cards in the hand of the player
     */
    public void setDrawOfCards(List<KitCard> drawOfCards) {
        this.drawOfCards = drawOfCards;
    }



}
