package model;

import cardgame.Strategy;
import controller.Game;
import model.Player;

import java.util.Random;

public class NaiveStrategy implements Strategy {
    @Override
    public String getName() {
        return "NaivePlayerBot";
    }


    //he just gives a random card, never a nope when he could
    //not THAT naive, you know what i mean

    @Override
    public int determineMove(Game game, Player player) {
        Random random = new Random();


        int randomMove = random.nextInt(player.getDrawOfCards().size());
        return randomMove;
    }

}
