package model;

import model.KitCard;

import javax.swing.plaf.PanelUI;
import java.util.*;

/**
 * CardDeck class, which is the class that handles the construction of a new deck of cards of a game of Exploding Kittens
 * and its functionalities
 */
public class CardDeck {
    private KitCard[] cards;
    private int cardsInDeck;


    /**
     * CardDeck constructors that initialises a bew card deck of type KitCard
     */
    public CardDeck() {
        cards = new KitCard[56];

    }

    /**
     * Method that takes the new card deck, cards and fills it with the required amount and types of cards needed for
     * a game of Exploding Kittens
     */
    public void reset() {

        KitCard.Type[] types = KitCard.Type.values();
        cardsInDeck = 0;

        for (int i = 0; i < 6; i++) {
            cards[cardsInDeck++] = new KitCard(KitCard.Type.DIFUSE);
        }
        for (int i = 0; i < 4; i++) {
            cards[cardsInDeck++] = new KitCard(KitCard.Type.EXPLODING_KITTEN);
        }
        for (int i = 0; i < 4; i++) {
            cards[cardsInDeck++] = new KitCard(KitCard.Type.ATTACK);
        }
        for (int i = 0; i < 4; i++) {
            cards[cardsInDeck++] = new KitCard(KitCard.Type.SKIP);
        }
        for (int i = 0; i < 4; i++) {
            cards[cardsInDeck++] = new KitCard(KitCard.Type.FAVOR);
        }
        for (int i = 0; i < 4; i++) {
            cards[cardsInDeck++] = new KitCard(KitCard.Type.SHUFFLE);
        }
        for (int i = 0; i < 5; i++) {
            cards[cardsInDeck++] = new KitCard(KitCard.Type.NOPE);
        }
        for (int i = 0; i < 5; i++) {
            cards[cardsInDeck++] = new KitCard(KitCard.Type.SEE_THE_FUTURE);
        }
        for (int i = 0; i < 4; i++) {
            cards[cardsInDeck++] = new KitCard(KitCard.Type.NORMAL_ONE);
        }
        for (int i = 0; i < 4; i++) {
            cards[cardsInDeck++] = new KitCard(KitCard.Type.NORMAL_TWO);
        }
        for (int i = 0; i < 4; i++) {
            cards[cardsInDeck++] = new KitCard(KitCard.Type.NORMAL_THREE);
        }
        for (int i = 0; i < 4; i++) {
            cards[cardsInDeck++] = new KitCard(KitCard.Type.NORMAL_FOUR);
        }
        for (int i = 0; i < 4; i++) {
            cards[cardsInDeck++] = new KitCard(KitCard.Type.NORMAL_FIVE);
        }

    }


    /**
     * Method used for the setup of the players hand. It takes cards from the constructed deck of cards
     *
     * @return the first card from the deck
     * @requires the cards are not empty
     */
    public KitCard takeCard(){
        if (cardsInDeck > 0){
            return cards[--cardsInDeck];
        }
        else {
            System.out.println("No cards left");
            return null;
        }

    }

    /**
     * Method used to shuffle the initial deck of cards
     * It is used before adjusting it for the number of players entering the game
     */
    public void shufflethecards(){
        Collections.shuffle(Arrays.asList(this.cards));
    }

    /**
     * Verification for the status of the deck
     *
     * @return true if the deck does not have any cards in it
     * @return false if the cards care not empty
     */
    public boolean isEmpty(){
        return cardsInDeck == 0;
    }


    /**
     *
     * @return the new status of the deck without the drawn card
     * @requires card deck not empty
     */
    public KitCard drawNewCard(){
        if (cardsInDeck > 0){
            return cards[--cardsInDeck];
        } else {
            System.out.println("No cards left");
            return null;
        }
    }

    /**
     * Method used at the beginning of each game in order to eliminate all of the cards of type DIFUSE
     * and EXPLODING_KITTEN to make sure that none of the player have more then one difuse card, that will be
     * given to them after and that none of the players are in the possesion of a card of type EXPLODING_KITTEN
     *
     * @param cardType represents the type of the card that must be removed in all of the found instances withing the deck
     *                 of cards
     */
    public void removeForSetup(KitCard.Type cardType){

        int finalNo = 0;
        for (int index = 0; index < cardsInDeck; index++){
            if(!cards[index].getTypes().equals(cardType)){
                cards[finalNo] = cards[index];
                finalNo++;
            }
        }
        cardsInDeck=finalNo;

    }

    /**
     *
     * @return the current number of cards left in the card deck
     */
    public int getCurrentNumberOfCards(){
        return cardsInDeck;
    }


    //SEE_THE_FUTURE

    /**
     *
     * @return the first three cards from the deck
     * @requires deck not empty(which is not possible due to the presence of the EXPLODING_KITTEN card)
     */
    public List<KitCard> seeTheFuture(){
        List<KitCard> theFuture = new ArrayList<>();
        if (cardsInDeck == 1) {
            theFuture.add(cards[cardsInDeck - 1]);
        }
        else if (cardsInDeck == 2) {
            theFuture.add(cards[cardsInDeck - 1]);
            theFuture.add(cards[cardsInDeck - 2]);
        }
        else {
            for (int i = 0; i < 3 && (cardsInDeck - i - 1) >= 0; i++) {
                theFuture.add(cards[cardsInDeck - i - 1]);
            }
        }
        return theFuture;
    }

    /**
     * Method with the role of returning all of the cards in a deck
     *
     * @return all of the remaining cards of the deck
     * @return an empty list of the deck is empty
     */
    public List<KitCard> seeAllCardsInDeck() {
        List<KitCard> allCards = new ArrayList<>();
        if(!isEmpty()){
            for (int i = 0; i < cardsInDeck; i++) {
                allCards.add(cards[i]);
            }
        }
        return allCards;
    }


    /**
     * Getter for the cards in a deck
     *
     * @return the cards of type KitCard from a deck
     */
    public KitCard[] getCards() {
        return this.cards;
    }

    /**
     * Method with the role of retrieving the first card in the deck
     * Used to verify wheather the next card will be an EXPLODING_KITTEN in the method turnOfPlayer in the Game class
     *
     * @return the first card that will be drawn by a player during the end of his turn
     * @requires cardsindeck > 0
     */
    public KitCard.Type seeABitOfTheFuture(){
        if (cardsInDeck > 0){
            return cards[cardsInDeck - 1].getTypes();

        }
        else
        {
            System.out.println("Nothing to see here");
            return null;
        }
    }

    /**
     * Method used to insert an EXPLODING_KITTEN by the player after he managed to difuse it
     *
     * @param card represents the card of type KitCard that will be inserted in the deck
     * @param chosenPosition represents the position in which the card will be added to the deck of cards.
     */
    public void insertExploding(KitCard card, int chosenPosition){
        if (chosenPosition < 0 || chosenPosition > cardsInDeck){
            System.out.println("Please chose a valid index.");
        }

        if (cardsInDeck == cards.length){
            cards = Arrays.copyOf(cards, cards.length + 1);
        }
        for (int i = cardsInDeck; i> chosenPosition; i--){
            cards[i] = cards[i-1];
        }

        cards[chosenPosition] = card;
        cardsInDeck++;

    }
    public void insertForServer(int index){
        KitCard card = new KitCard(KitCard.Type.EXPLODING_KITTEN);
        if (index < 0 || index > cardsInDeck){
            System.out.println("Please chose a valid index.");
        }

        if (cardsInDeck == cards.length){
            cards = Arrays.copyOf(cards, cards.length + 1);
        }
        for (int i = cardsInDeck; i> index; i--){
            cards[i] = cards[i-1];
        }

        cards[index] = card;
        cardsInDeck++;


    }

    /**
     *Method used to add a specific number of cards of a specific type back to the card deck
     * Used after the setup of the players hand, it adds the required amount of cards of type EXPLODING_KITTEN and DIFUSE
     * back the deck, depending on the number of players entering the game
     *
     * @param cardType represents the Type of card that shall be added in the deck
     * @param count represents the number if the specified Type that will be added in the deck
     */
    public void addCardsToTheDeck(KitCard.Type cardType, int count) {
        for (int i = 0; i < count; i++) {
            if (cardsInDeck >= cards.length) {
                cards = Arrays.copyOf(cards, cards.length + 1);
            }
            cards[cardsInDeck++] = new KitCard(cardType);
        }
    }

    /**
     *Method used to shuffle the cards in a deck of cards after the set up was done and the required cards were added
     * back ti the deck,
     *
     * The method is also used for handling the use of a card of type SHUFFLE
     *
     */
    public void shuffleAfterSetUp() {
        Random rnd = new Random();
        for (int i = this.cardsInDeck - 1; i > 0; i--) {
            int index = rnd.nextInt(i + 1);
            KitCard a = this.cards[index];
            this.cards[index] = cards[i];
            this.cards[i] = a;
        }
    }
}


