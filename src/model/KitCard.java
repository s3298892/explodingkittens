package model;

public class KitCard {

    public enum Type {
        DIFUSE, ATTACK, EXPLODING_KITTEN, FAVOR, NOPE, NORMAL_ONE, SEE_THE_FUTURE, SHUFFLE, SKIP, NORMAL_TWO, NORMAL_THREE, NORMAL_FOUR, NORMAL_FIVE;
        private static final Type[] types = Type.values();
        private static Type getType(int i){
            return Type.types[i];
        }
    }
    private final Type types;
    public KitCard(final Type types){
        this.types = types;
    }
    public Type getTypes(){
        return this.types;
    }


    public String toString() {
        return "Card of type "+types;
    }
}


