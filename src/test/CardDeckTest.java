package test;


import model.CardDeck;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.*;

    public class CardDeckTest {

        private CardDeck deck;

        @BeforeEach
        public void setUp(){
            deck = new CardDeck();
        }

        @Test
        public void TestReset(){
            CardDeck originalDeck = new CardDeck();
            deck.reset();
            assertNotEquals(deck.getCards(), originalDeck.getCards());
        }

        @Test
        public void TestShuffle(){
            CardDeck originalDeck = new CardDeck();
            deck.shufflethecards();
            assertNotEquals(deck.getCards(), originalDeck.getCards());
        }

        @Test
        public void TestIsEmpty(){
            int cards = 56;
            CardDeck emptyDeck = new CardDeck();

            while(cards!=0){
                cards--;
            }
            assertTrue(emptyDeck.isEmpty());
            assertTrue(deck.isEmpty());
        }


    }
