package test;

import model.CardDeck;
import controller.Game;
import model.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GameTest {

    private CardDeck deck;
    private Player[] players;

    @BeforeEach
    public void SetUp(){
        players = new Player[]{
                new Player("Mihai"),
                new Player("Gia")
        };

    }

    @Test
    public void testIsGameOverWithOnePlayer() {
        for (Player player : players){
            if (player.getPlayerName().equals("Mihai")){
                player.eliminate();
            }
        }

        Game game = new Game(players);
        assertTrue(game.isGameOver());
    }

    @Test
    public void testIsGameOverWithTwoPlayers() {
        Game game = new Game(players);
        assertFalse(game.isGameOver());
    }

}
