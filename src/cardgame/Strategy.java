package cardgame;

import controller.Game;
import model.Player;

public interface Strategy {

    public String getName();

    public int determineMove(Game game, Player player);

}
