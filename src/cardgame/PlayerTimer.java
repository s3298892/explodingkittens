package cardgame;

import controller.Game;
import model.Player;

import java.util.Timer;
import java.util.TimerTask;

public class PlayerTimer {
    private Timer turnTimer;
    private int numberOfSeconds;
    private Player playersTurn;
    private Game game;

    public PlayerTimer(int numberOfSeconds, Player playersTurn, Game game) {

        this.numberOfSeconds = numberOfSeconds;
        this.playersTurn = playersTurn;
        this.game = game;
    }
    public void startTheTimer(){
        turnTimer = new Timer();
        turnTimer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                if (numberOfSeconds>0){
                    System.out.println("Player "+ playersTurn.getPlayerName()+ ", you have  "+ numberOfSeconds+ " left.");
                    numberOfSeconds--;

                }

            }
        }, 0, 1000);

    }

    public void theTimeIsOutPlayer(){
        /**
         * make him draaw a card, a separate method in game
         */
    }
    public void cancelWhenDone(){
        if (turnTimer!=null){
            turnTimer.cancel();
        }
    }


}
