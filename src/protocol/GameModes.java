package protocol;

public enum GameModes {
    N, // Normal
    T, // Teamplay
    L, // Lobby
    S, // Streaking kittens
    C  //Combos Enabled
}