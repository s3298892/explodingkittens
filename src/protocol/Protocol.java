package protocol;


public class Protocol {

    // Utils
    public static final String SEPARATOR = "|";

    // Errors
    public enum Error{
        TAKEN_USERNAME("Name is already in use. Please try again."),
        INVALID_USERNAME("Usernames may not contain '|'. Please try again."),
        INVALID_INDEX("Index is invalid or out of bounds. Please try again."),
        INVALID_COMMAND("Unknown command. Please try again."),
        INVALID_MOVE("This move is invalid. Please try again."),
        INVALID_ARGUMENT("Invalid argument(s). Please try again."),
        CLIENT_DISCONNECTION("Client disconnected. Please try again."),
        SERVER_DISCONNECTION("Server disconnected. Please try again.");

        private String description;
        public String getDescription() {
            return this.description;
        }

        Error(String description) {
            this.description = description;
        }
    }

    // All commands
    public interface Command {}
    public enum BasicCommand implements Command {
        ANNOUNCE,
        WELCOME,
        REQUEST_GAME,
        INFORM_QUEUE,
        START_GAME,
        CURRENT_PLAYER,
        GAME_STATE,
        PLAY,
        PASS,
        PICK,
        CHAT,
        PRINT_CHAT,
        DRAW,
        PRINT_DRAW,
        PRINT_HAND,
        INFORM_MOVE,
        GAME_OVER,
        START,
        CONFIRM_START
    }
}
