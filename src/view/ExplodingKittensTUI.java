package view;

import controller.Game;
import model.Player;

import java.util.Scanner;

public class ExplodingKittensTUI {

    private Game game;
    private Player[] players;
    private Scanner scanner;


    public ExplodingKittensTUI() {
        this.scanner = new Scanner(System.in);

    }

//Game Instructions



    public Player[] startPlayers(){

        System.out.println("How many players will participate in our game today?");
        System.out.println("Please enter a number from 2 to 5");


        while (true){
            int playersChoice = scanner.nextInt();
            if (playersChoice == 2){

                this.players = new Player[playersChoice];

                System.out.println("Player 1 please enter your name: ");
                String namePlayer1 = scanner.next();

                System.out.println("Welcome te the game "+namePlayer1);
                System.out.println("Player 2 please enter your name: ");

                String namePlayer2 = scanner.next();
                System.out.println("Welcome to the game "+ namePlayer2);

                Player player1 = new Player(namePlayer1);
                Player player2 = new Player(namePlayer2);

                players[0] = player1;
                players[1] = player2;
                break;



            }
            else if (playersChoice == 3){
                this.players = new Player[playersChoice];

                System.out.println("Player 1 please enter your name: ");
                String namePlayer1 = scanner.next();
                System.out.println("Welcome te the game "+namePlayer1);

                System.out.println("Player 2 please enter your name: ");
                String namePlayer2 = scanner.next();
                System.out.println("Welcome to the game "+ namePlayer2);

                System.out.println("Player 3 please enter your name: ");
                String namePlayer3 = scanner.next();
                System.out.println("Welcome to the game "+ namePlayer3);

                Player player1 = new Player(namePlayer1);
                Player player2 = new Player(namePlayer2);
                Player player3 = new Player(namePlayer3);


                players[0] = player1;
                players[1] = player2;
                players[2] = player3;
                break;


            }
            else if (playersChoice == 4){
                this.players = new Player[playersChoice];

                System.out.println("Player 1 please enter your name: ");
                String namePlayer1 = scanner.next();
                System.out.println("Welcome te the game "+namePlayer1);

                System.out.println("Player 2 please enter your name: ");
                String namePlayer2 = scanner.next();
                System.out.println("Welcome to the game "+ namePlayer2);

                System.out.println("Player 3 please enter your name: ");
                String namePlayer3 = scanner.next();
                System.out.println("Welcome to the game "+ namePlayer3);

                System.out.println("Player 4 please enter your name: ");
                String namePlayer4 = scanner.next();
                System.out.println("Welcome to the game "+ namePlayer4);

                Player player1 = new Player(namePlayer1);
                Player player2 = new Player(namePlayer2);
                Player player3 = new Player(namePlayer3);
                Player player4 = new Player(namePlayer4);


                players[0] = player1;
                players[1] = player2;
                players[2] = player3;
                players[3] = player4;
                break;

            }
            else if (playersChoice == 5){
                this.players = new Player[playersChoice];

                System.out.println("Player 1 please enter your name: ");
                String namePlayer1 = scanner.next();
                System.out.println("Welcome te the game "+namePlayer1);

                System.out.println("Player 2 please enter your name: ");
                String namePlayer2 = scanner.next();
                System.out.println("Welcome to the game "+ namePlayer2);

                System.out.println("Player 3 please enter your name: ");
                String namePlayer3 = scanner.next();
                System.out.println("Welcome to the game "+ namePlayer3);

                System.out.println("Player 4 please enter your name: ");
                String namePlayer4 = scanner.next();
                System.out.println("Welcome to the game "+ namePlayer4);

                System.out.println("Player 5 please enter your name: ");
                String namePlayer5 = scanner.next();
                System.out.println("Welcome to the game "+ namePlayer5);

                Player player1 = new Player(namePlayer1);
                Player player2 = new Player(namePlayer2);
                Player player3 = new Player(namePlayer3);
                Player player4 = new Player(namePlayer4);
                Player player5 = new Player(namePlayer5);


                players[0] = player1;
                players[1] = player2;
                players[2] = player3;
                players[3] = player4;
                players[4] = player5;

                break;

            }
            else {
                System.out.println("Please enter a valid number.");
            }

        }
        return players;
    }

    public void gameRules(){
        System.out.println("Welcome, welcome, we embark to our exploding kittens game, but not before, giving you the rules for");
        System.out.println("this nuclear game");
        System.out.println("Each of you players will have a hand consisting of 7 cards, of which one is a DIFUSE");
        System.out.println("(that will be adressed later). ");
        System.out.println(" ");


        System.out.println("Good luck, and may the odds be ever in your favor.");
        System.out.println(" ");
    }

    public void start() {
        this.players = startPlayers();
        this.game = new Game(players);
        gameRules();
        game.play();
    }







    public static void main(String[] args) {

        String kitty =
                " /\\_/\\   /\\_/\\   /\\_/\\   /\\_/\\   /\\_/\\ \n" +
                        "( o.o ) ( o.o ) ( o.o ) ( o.o ) ( o.o )\n" +
                        " > ^ <   > ^ <   > ^ <   > ^ <   > ^ < ";
        String poster = "   ('-.  ) (`-.       _ (`-.                        _ .-') _                .-') _                  .-. .-')           .-') _    .-') _     ('-.       .-') _   .-')    \n" +
                " _(  OO)  ( OO ).    ( (OO  )                      ( (  OO) )              ( OO ) )                 \\  ( OO )         (  OO) )  (  OO) )  _(  OO)     ( OO ) ) ( OO ).  \n" +
                "(,------.(_/.  \\_)-._.`     \\ ,--.      .-'),-----. \\     .'_   ,-.-') ,--./ ,--,'  ,----.          ,--. ,--.  ,-.-') /     '._ /     '._(,------.,--./ ,--,' (_)---\\_) \n" +
                " |  .---' \\  `.'  /(__...--'' |  |.-') ( OO'  .-.  ',`'--..._)  |  |OO)|   \\ |  |\\ '  .-./-')       |  .'   /  |  |OO)|'--...__)|'--...__)|  .---'|   \\ |  |\\ /    _ |  \n" +
                " |  |      \\     /\\ |  /  | | |  | OO )/   |  | |  ||  |  \\  '  |  |  \\|    \\|  | )|  |_( O- )      |      /,  |  |  \\'--.  .--''--.  .--'|  |    |    \\|  | )\\  :` `.  \n" +
                "(|  '--.    \\   \\ | |  |_.' | |  |`-' |\\_) |  |\\|  ||  |   ' |  |  |(_/|  .     |/ |  | .--, \\      |     ' _) |  |(_/   |  |      |  |  (|  '--. |  .     |/  '..`''.) \n" +
                " |  .--'   .'    \\_)|  .___.'(|  '---.'  \\ |  | |  ||  |   / : ,|  |_.'|  |\\    | (|  | '. (_/      |  .   \\  ,|  |_.'   |  |      |  |   |  .--' |  |\\    |  .-._)   \\ \n" +
                " |  `---. /  .'.  \\ |  |      |      |    `'  '-'  '|  '--'  /(_|  |   |  | \\   |  |  '--'  |       |  |\\   \\(_|  |      |  |      |  |   |  `---.|  | \\   |  \\       / \n" +
                " `------''--'   '--'`--'      `------'      `-----' `-------'   `--'   `--'  `--'   `------'        `--' '--'  `--'      `--'      `--'   `------'`--'  `--'   `-----'  ";

        System.out.println(poster+"\n"+kitty);

        ExplodingKittensTUI explodingTui = new ExplodingKittensTUI();
        explodingTui.start();

        //Option to exit the game to be implemented, in meniu acolo la turn of player


    }
}
