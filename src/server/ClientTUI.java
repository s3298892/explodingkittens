package server;


import protocol.Protocol;

import java.net.InetAddress;
import java.util.Scanner;

public class ClientTUI {
// TODO: switch command for what goes to the server to the client, the client handles takes the method start game and sends the command, and confirms to the
//     player that the game has started
//
// fiecare comand a de la server RG de la cliend, client handlerul ia metoda start game si trimite comanda sg, confirmare la player ca a inceput jocul

    private Client client;

    public ClientTUI (Client client){

        this.client = client;

    }

    public void sendTo(String msg){
        System.out.println(msg);
    }

    public String receiveInput(){
        Scanner command = new Scanner(System.in);
        return command.nextLine();
    }

    public void kittyMsg(){
        String kitty =
                " /\\_/\\   /\\_/\\   /\\_/\\   /\\_/\\   /\\_/\\ \n" +
                        "( o.o ) ( o.o ) ( o.o ) ( o.o ) ( o.o )\n" +
                        " > ^ <   > ^ <   > ^ <   > ^ <   > ^ < ";
        String poster = "   ('-.  ) (`-.       _ (`-.                        _ .-') _                .-') _                  .-. .-')           .-') _    .-') _     ('-.       .-') _   .-')    \n" +
                " _(  OO)  ( OO ).    ( (OO  )                      ( (  OO) )              ( OO ) )                 \\  ( OO )         (  OO) )  (  OO) )  _(  OO)     ( OO ) ) ( OO ).  \n" +
                "(,------.(_/.  \\_)-._.`     \\ ,--.      .-'),-----. \\     .'_   ,-.-') ,--./ ,--,'  ,----.          ,--. ,--.  ,-.-') /     '._ /     '._(,------.,--./ ,--,' (_)---\\_) \n" +
                " |  .---' \\  `.'  /(__...--'' |  |.-') ( OO'  .-.  ',`'--..._)  |  |OO)|   \\ |  |\\ '  .-./-')       |  .'   /  |  |OO)|'--...__)|'--...__)|  .---'|   \\ |  |\\ /    _ |  \n" +
                " |  |      \\     /\\ |  /  | | |  | OO )/   |  | |  ||  |  \\  '  |  |  \\|    \\|  | )|  |_( O- )      |      /,  |  |  \\'--.  .--''--.  .--'|  |    |    \\|  | )\\  :` `.  \n" +
                "(|  '--.    \\   \\ | |  |_.' | |  |`-' |\\_) |  |\\|  ||  |   ' |  |  |(_/|  .     |/ |  | .--, \\      |     ' _) |  |(_/   |  |      |  |  (|  '--. |  .     |/  '..`''.) \n" +
                " |  .--'   .'    \\_)|  .___.'(|  '---.'  \\ |  | |  ||  |   / : ,|  |_.'|  |\\    | (|  | '. (_/      |  .   \\  ,|  |_.'   |  |      |  |   |  .--' |  |\\    |  .-._)   \\ \n" +
                " |  `---. /  .'.  \\ |  |      |      |    `'  '-'  '|  '--'  /(_|  |   |  | \\   |  |  '--'  |       |  |\\   \\(_|  |      |  |      |  |   |  `---.|  | \\   |  \\       / \n" +
                " `------''--'   '--'`--'      `------'      `-----' `-------'   `--'   `--'  `--'   `------'        `--' '--'  `--'      `--'      `--'   `------'`--'  `--'   `-----'  ";

        System.out.println(poster+"\n"+kitty);
    }

    public void start(){
        try{
            while (true){
                String userInput = receiveInput();
                handleInput(userInput);
            }
        } catch (Exception e){
            sendTo("Error: " + e);
        }
    }

    public void handleInput(String msg){
        String[] splitInput;
        String param = null, cmd;
        splitInput = msg.split(" ");
        cmd = splitInput[0];
        if (splitInput.length >= 2)
            param = splitInput[1];
        switch (cmd) {
            case "REQUEST_GAME":
                try {
                    client.sendMessage(String.valueOf(Protocol.BasicCommand.REQUEST_GAME));
                }catch (IllegalArgumentException e){
                    System.out.println("error: " + e);
                }
                break;
            default:
                System.out.println(cmd);
                System.out.println("Wrong command.");
        }
    }

    public InetAddress getIpAdress(){
        while (true){
            try {
                sendTo("Enter a server IP adress: ");
                String userInput = receiveInput();
                return InetAddress.getByName(userInput);
            } catch (Exception e){
                sendTo("The Ip adress introduced is invalid. Please try again.");
            }
        }
    }
}