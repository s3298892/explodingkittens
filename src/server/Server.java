package server;

import controller.Game;
import model.Player;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server implements Runnable{

    private ServerSocket ssock;
    public static List<ClientHandler> clients;

    private int next_client_no;
    private View view;
    private Player[] playersForGame;


    public Server(){
        clients=new ArrayList<>();
        view=new View();
        next_client_no=1;
        playersForGame = new Player[]{new Player("Fake player", 0), new Player("Fake player2", 1),
                new Player("Fake player3", 3)};
    }


    public void run(){
        boolean openNewSocket=true;
        while(openNewSocket){
            try {
                setup();
                while(true){
                    Socket sock=ssock.accept();
                    String name="Client "+String.format("%02d",next_client_no++);
                    view.update("New client ["+name+"] connected!");
                    ClientHandler handler = new ClientHandler(sock,this,name);
                    System.out.println(sock);
                    new Thread(handler).start();
                    clients.add(handler);
                }

            } catch (IOException e1) {
                openNewSocket=false;
//            } catch (InterruptedException e) {
//                System.out.println("A server IO error occurred: "+e.getMessage());
//            }
        }
        view.update("See you later!");
    }

    public void setup() throws IllegalArgumentException {

        ssock=null;
        while(ssock==null){
            view.update("Please enter the server port.");
            int port = 1235;

            try {
                view.update("Attempting to open a socket at 127.0.0.1 "+"on port "+port+"...");
                ssock=new ServerSocket(port,0, InetAddress.getByName("localhost"));
                view.update("Server started at port "+port);
            } catch (IOException e) {
                view.update("ERROR: could not create a socket on "+"127.0.0.1"+" and port "+port+".");
            }
        }
    }

    public void startGame(){
        System.out.println("Starting the game...");
        int i = 0;
        for(ClientHandler clientHandler : clients){
            playersForGame[i] = new Player(clientHandler.getName(), i);
            i++;
        }
        Game gameObject = new Game(playersForGame);
        Thread gameThread = new Thread(gameObject);
        gameThread.start();
    }

    public void removeClient(ClientHandler client){
        this.clients.remove(client);
    }
    public static void main(String[] args) {
        Server server = new Server();
        System.out.println("Hello world");
        new Thread(server).start();
    }
}
