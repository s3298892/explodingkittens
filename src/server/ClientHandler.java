package server;

import protocol.Protocol;

import java.io.*;
import java.net.Socket;


public class ClientHandler implements Runnable {
    private BufferedReader in;
    private BufferedWriter out;
    private Socket sock;
    private Server srv;
    private String name;

    public ClientHandler(Socket sock, Server srv, String name) {
        try {
            in = new BufferedReader(
                    new InputStreamReader(sock.getInputStream()));
            out = new BufferedWriter(
                    new OutputStreamWriter(sock.getOutputStream()));
            this.sock = sock;
            this.srv = srv;
            this.name = name;
        } catch (IOException e) {
            shutdown();
        }
    }

    public String getName() {
        return name;
    }


    public void run() {
        String msg;
        try {
            while ((msg = in.readLine()) != null) {
                System.out.println("> [" + name + "] Incoming: " + msg);
                handleCommand(msg);
                out.newLine();
                out.flush();
                //msg = in.readLine();
            }
            shutdown();
        } catch (IOException e) {
            shutdown();
        }
    }


    private void handleCommand(String input) throws IOException {
        String[] splitInput;
        String param = null, cmd;
        splitInput = input.split(" ");
        cmd = splitInput[0];
        if (splitInput.length >= 2)
            param = splitInput[1];
        switch (cmd) {
            case "REQUEST_GAME":
                sendMessageToClient("Game is starting.");
                srv.startGame();
                break;
            default:
                sendMessageToClient("Wrong command.");
        }
    }

    public void sendMessageToClient(String msg) {
        try {
            out.write(msg);
            out.newLine();
            out.flush();
        } catch (IOException e) {
            System.out.println("error: " + e);;
        }
    }

    public void shutdown() {
        System.out.println("> [" + name + "] Shutting down.");
        try {
            in.close();
            out.close();
            sock.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        srv.removeClient(this);
    }
}
