package server;

import java.util.Scanner;

public class View {

    public void update(String msg){
        System.out.println(msg);
    }

    public String receiveInput(){
        Scanner command = new Scanner(System.in);
        return command.nextLine();
    }
}
