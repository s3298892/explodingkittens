package controller;

import model.CardDeck;
import model.KitCard;
import model.Player;
import server.ClientHandler;
import server.Server;

import java.util.*;

/**
 *Game class
 * responsible for the beginning of each new game of Exploding Kittens
 * It handles their hand of cards, their turns, eliminations, end of a game
 */
public class Game implements Runnable{
    private final Player[] players;
    private final CardDeck deck;
    private final ArrayList<KitCard> pile;
    private final Scanner scanner;

    /**
     * Constructor for the Game class
     *
     * It initialises a new CardDeck for the game and shuffles the cards,
     *
     * It initialises the card pile as a new ArrayList, which will be filled with the cards that were
     * already played by the players.
     *
     * It initialises the scanner through which the players will be able to give commands in the console.
     *
     * It is responsible for giving each player the required amount of cards and a card of type DIFUSE for a complete
     * hand of cards before the game begins.
     *
     * @param players represents an ArrayList of the players which will play the game.
     */
    public Game(Player[] players){
        deck=new CardDeck();
        deck.reset();
        deck.shufflethecards();
        pile= new ArrayList<>();
        this.players = players;
        this.scanner = new Scanner(System.in);

        for (Player player : players){
            deck.removeForSetup(KitCard.Type.EXPLODING_KITTEN);
            deck.removeForSetup(KitCard.Type.DIFUSE);

            ArrayList<KitCard> hand = new ArrayList<KitCard>();
            for (int i = 0; i < 7; i++) {
                hand.add(deck.takeCard());
            }

            hand.add(new KitCard(KitCard.Type.DIFUSE));
            player.setDrawOfCards(hand);

        }

    }

    /**
     * Getter for the pile
     *
     * @return the cards in the pile
     */
    public ArrayList<KitCard> getPile() {
        return this.pile;
    }

    /**
     *Method used to verify whether the game is over or not.
     * It iterates through the players to see how many are eliminated and counts them
     *
     * @return true if the game is over, meaning that only one player is left in the game,
     * false if there are at least two players left in the game
     *
     */
    public boolean isGameOver(){
        int playersLeft = 0;
        for (Player player : players){
            if (!player.isEliminatedPlayer()){
                playersLeft++;
            }
        }
        return playersLeft ==1;
    }


    /**
     * Method that has the role of determining the winner of the round, which is done at the end of the game
     * It iterates through the players and displays the player that is the only one that was not eliminated
     */
    public void winner() {
        if (isGameOver()) {
            for (Player player : players) {
                if (!player.isEliminatedPlayer()) {
                    showThisToAll("Player " + player.getPlayerName() + " is the winner!");
                }
            }
        }
    }

    @Override
    public void run() {
        play();
    }

    public void showThisToAll(String msg){
        for(ClientHandler clientHandler : Server.clients){
            clientHandler.sendMessageToClient(msg);
        }
    }

    /**
     *
     * @param scanner
     * @return
     */
    public int getPlayerChoice (Scanner scanner){
        while (true){
            try
            {
                int playerChoice = scanner.nextInt();
                scanner.nextLine();
                return playerChoice;
            } catch (Exception e)
            {
                showThisToAll("Please select from the given choices.");
                scanner.nextLine();
            }
        }
    }


    //get a String from the player
    public String getStringChoice (Scanner scanner){
        while (true){
            try{
                String choice = scanner.nextLine().trim().toLowerCase();
                if (!choice.isEmpty()){
                    return choice;
                }
                else {
                    showThisToAll("Choice can not be empty");
                }
            } catch (Exception e) {
                showThisToAll("Error, please enter another choice.");
                scanner.nextLine();
            }
        }
    }


    /**
     * Method used to verify the validity of the cards chosen by a player who wants to play a 2 cards combo
     *
     * @requires that the player does not choose the same card two times and both cards are part of his hand of cards
     *
     * @param firstCard represents the first chosen card
     * @param secondCard represents the second chosen card
     * @param player represents the player which wished to play the 2 cards combo
     * @return true if the cards are valid, false otherwise
     */
    public boolean secondComboValidity (int firstCard, int secondCard, Player player){
        return firstCard >= 0 && firstCard < player.getDrawOfCards().size() && secondCard >= 0 && secondCard < player.getDrawOfCards().size() && firstCard != secondCard;
    }


    /**
     * Method used to verify the validity of the chosen cards for a 5 cards combo
     *
     * @requires that the cards are in the bound of the hand of the player and that the player does not make the same
     * choice more than one time, that means no duplicates
     *
     * @param firstCard the first chosen card for the combo
     * @param secondCard the second chosen card for the combo
     * @param third the third chosen card for the combo
     * @param fourth the fourth chosen card for the combo
     * @param fifth the fifth chosen card for the combo
     * @param player represents the player who wished to play the 5 cards combo
     *
     * @return true if the chosen cards are in the players hand bounds and there are no duplicates, false otherwise
     */
    public boolean fiveCardsComboValidity(int firstCard, int secondCard, int third, int fourth, int fifth, Player player){
        if (firstCard >= 0 && firstCard < player.getDrawOfCards().size() && secondCard >= 0 && secondCard < player.getDrawOfCards().size() && third >= 0 && third < player.getDrawOfCards().size() && fourth >= 0 && fourth < player.getDrawOfCards().size() && fifth >= 0 && fifth < player.getDrawOfCards().size()){
            Set<Integer> verifyDoubles = new HashSet<>(Arrays.asList(firstCard,secondCard,third,fourth, fifth));
            return verifyDoubles.size() == 5;
        }
        return false;
    }


    //verification cond 3 cards combo, no duplivates or out of bounds

    /**
     * Method used to verofy the validity of the chosen cards of a player in the method turnOfPlayer for a player
     * who wishes to play a three cards combo
     *
     * @param first the first chosen card for the combo
     * @param second the second chosen card for the combo
     * @param third the third chosen card for the combo
     * @param player represents the player who wished to play the 3 cards combo
     *
     * @return true if the combo is valid, false otherwise
     */
    public boolean threeCardsComboValidity(int first, int second, int third, Player player){
        if (first >= 0 && first < player.getDrawOfCards().size() && second >= 0 && second < player.getDrawOfCards().size() && third >= 0 && third < player.getDrawOfCards().size()){
            Set<Integer> verifyDoubles = new HashSet<>(Arrays.asList(first, second, third));
            return verifyDoubles.size() == 3;
        }
        return false;
    }


    /**
     * TODO: add a while to the method
     *
     * Method used when the action of a card of type FAVOR is being played,
     * @param playerWhoRequests
     */
    public void favorRequest (Player playerWhoRequests){
        showThisToAll("Please select the player from which you want a card(only the index)");
        for (int i = 0; i < players.length; i++){
            if (!players[i].getPlayerName().equals(playerWhoRequests.getPlayerName())){
                if(!players[i].getDrawOfCards().isEmpty()){
                    showThisToAll(i+ " "+ players[i].getPlayerName());
                }
            }
        }
        int targetedPlayerInt = getPlayerChoice(scanner);

        if(targetedPlayerInt >= 0 && targetedPlayerInt < players.length && !players[targetedPlayerInt].getPlayerName().equals(playerWhoRequests.getPlayerName())){
            Player targetedPlayer = players[targetedPlayerInt];
            showThisToAll("Valid target. Player "+ targetedPlayer.getPlayerName() + " select a card to give (only the index)");
            for (int i = 0; i< targetedPlayer.getDrawOfCards().size(); i++){
                showThisToAll(i+ " "+ targetedPlayer.getDrawOfCards().get(i).getTypes().toString());
            }
            int cardToGive = getPlayerChoice(scanner);

            if (cardToGive >= 0 && cardToGive<targetedPlayer.getDrawOfCards().size()){
                playerWhoRequests.getDrawOfCards().add(targetedPlayer.getDrawOfCards().get(cardToGive));
                targetedPlayer.getDrawOfCards().remove(cardToGive);
                showThisToAll("Favor move successful!");
            }
            else {
                showThisToAll("Invalid card index.");
            }

        }
        else {
            showThisToAll("iNVALID TARGET");
        }


    }



    /**
     * Method which plays out the three cards combo logic, it is called in the turnOfPlayer method, oce the cards of the
     * player who wishes to play the combo has given valid cards required for this action.
     *
     * The method asks the player to choose a target from the players in the game and the Type of card he wishes to acquire
     * from the targeted player. If the targeted player is not in the possession of a card of the required Type, then the
     * player who is playing the combo gets nothing.
     *
     * If the player who requests the combo does not choose from the given choices, he has to continue the round and
     * select another card to play, or try to play the combo again.
     *
     *
     * @requires that the player only chooses from the given choices, otherwise the combo will fail
     * @param playerWhoRequests represents the player who currently wants to play a 3 cards combo
     *
     */
    public void threeCardsCombo(Player playerWhoRequests){
        showThisToAll("Please select from the pool of players (only the index): ");
        for (int i = 0; i < players.length; i++){
            if (!players[i].getPlayerName().equals(playerWhoRequests.getPlayerName())){
                showThisToAll(i+ " "+ players[i].getPlayerName());
            }
        }

        int targetedPlayerInt = getPlayerChoice(scanner);

        if(targetedPlayerInt >= 0 && targetedPlayerInt < players.length && !players[targetedPlayerInt].getPlayerName().equals(playerWhoRequests.getPlayerName())){
            Player targetedPlayer = players[targetedPlayerInt];

            showThisToAll("Valid target. Now please choose the card you want from " + players[targetedPlayerInt].getPlayerName());
            showThisToAll("Please select ONLY the number ");
            showThisToAll("Valid choices: 1) SKIP, 2) FAVOR, 3) NOPE, 4) DIFUSE, 5) ATTACK");
            showThisToAll("6) NORMAL_ONE, 7) NORMAL_TWO, 8) NORMAL_THREE, 9) NORMAL_FOUR, 10) NORMAL_FIVE");


            int cardChoice = getPlayerChoice(scanner);

            switch (cardChoice){
                case 1:
                    KitCard cardSkip = new KitCard(KitCard.Type.SKIP);
                    if (targetedPlayer.getDrawOfCards().contains(cardSkip)){
                        playerWhoRequests.getDrawOfCards().add(cardSkip);
                        targetedPlayer.getDrawOfCards().remove(cardSkip);
                    }
                    else {
                        showThisToAll("The player does not have the card");
                    }
                    break;

                case 2:
                    KitCard cardFavor = new KitCard(KitCard.Type.FAVOR);
                    if (targetedPlayer.getDrawOfCards().contains(cardFavor)){

                        playerWhoRequests.getDrawOfCards().add(cardFavor);
                        targetedPlayer.getDrawOfCards().remove(cardFavor);
                    }
                    else {
                        showThisToAll("The player does not have the card");
                    }
                    break;

                case 3:
                    KitCard cardNope = new KitCard(KitCard.Type.NOPE);
                    if (targetedPlayer.getDrawOfCards().contains(cardNope)){

                        playerWhoRequests.getDrawOfCards().add(cardNope);
                        targetedPlayer.getDrawOfCards().remove(cardNope);
                    }
                    else {
                        showThisToAll("The player does not have the card");
                    }
                    break;

                case 4:
                    KitCard cardDifuse = new KitCard(KitCard.Type.DIFUSE);
                    if (targetedPlayer.getDrawOfCards().contains(cardDifuse)){

                        playerWhoRequests.getDrawOfCards().add(cardDifuse);
                        targetedPlayer.getDrawOfCards().remove(cardDifuse);
                    }
                    else {
                        showThisToAll("The player does not have the card");
                    }
                    break;
                case 5:
                    KitCard cardAttack = new KitCard(KitCard.Type.ATTACK);
                    if (targetedPlayer.getDrawOfCards().contains(cardAttack)){

                        playerWhoRequests.getDrawOfCards().add(cardAttack);
                        targetedPlayer.getDrawOfCards().remove(cardAttack);
                    }
                    else {
                        showThisToAll("The player does not have the card");
                    }
                    break;

                case 6:
                    KitCard cardNormalOne = new KitCard(KitCard.Type.NORMAL_ONE);
                    if (targetedPlayer.getDrawOfCards().contains(cardNormalOne)){

                        playerWhoRequests.getDrawOfCards().add(cardNormalOne);
                        targetedPlayer.getDrawOfCards().remove(cardNormalOne);
                    }
                    else {
                        showThisToAll("The player does not have the card");
                    }
                    break;

                case 7:
                    KitCard cardNormalTwo = new KitCard(KitCard.Type.NORMAL_TWO);
                    if (targetedPlayer.getDrawOfCards().contains(cardNormalTwo)){

                        playerWhoRequests.getDrawOfCards().add(cardNormalTwo);
                        targetedPlayer.getDrawOfCards().remove(cardNormalTwo);
                    }
                    else {
                        showThisToAll("The player does not have the card");
                    }
                    break;

                case 8:
                    KitCard cardNormalThree = new KitCard(KitCard.Type.NORMAL_THREE);
                    if (targetedPlayer.getDrawOfCards().contains(cardNormalThree)){

                        playerWhoRequests.getDrawOfCards().add(cardNormalThree);
                        targetedPlayer.getDrawOfCards().remove(cardNormalThree);
                    }
                    else {
                        showThisToAll("The player does not have the card");
                    }
                    break;

                case 9:
                    KitCard cardFNormalFour = new KitCard(KitCard.Type.NORMAL_FOUR);
                    if (targetedPlayer.getDrawOfCards().contains(cardFNormalFour)){

                        playerWhoRequests.getDrawOfCards().add(cardFNormalFour);
                        targetedPlayer.getDrawOfCards().remove(cardFNormalFour);
                    }
                    else {
                        showThisToAll("The player does not have the card");
                    }
                    break;

                case 10:
                    KitCard cardNormalFive = new KitCard(KitCard.Type.NORMAL_FIVE);
                    if (targetedPlayer.getDrawOfCards().contains(cardNormalFive)){

                        playerWhoRequests.getDrawOfCards().add(cardNormalFive);
                        targetedPlayer.getDrawOfCards().remove(cardNormalFive);
                    }
                    else {
                        showThisToAll("The player does not have the card");
                    }
                    break;

                default:
                    showThisToAll("Invalid request. please ONLY choose from the list");
            }
        }
        else {
            showThisToAll("invalid target");
        }
    }

    /**
     * Method which determines the actions that can be played during the turn of the player.
     *
     * This method gives him the actions he can do during his turn. He can either play from his hand of cards and choose
     * the number index of the card he wishes to play, or he can play one of the possible cards' combo. He would have to
     * write -2 for a two cards combo, -3 for a three cards combo, and -5 for a 5 cards combo. The end of each turn is
     * done by typing the command -1 and drawing a card, however if the player is attacked, he will draw a card and
     * continue to the next turn again, without ending until he plays another attack of the required number of turns
     * are being played.
     *
     * For each card that represents an actual action in the game, such as SEE_THE_FUTURE, ATTACK, SKIP etc, the other
     * players will be asked if they wish to NOPE the action and the result of the NOPE question will determine whether the
     * action will be player or not
     *
     * When the player decides to draw a card, typing -1, the method will handle the case of an EXPLODING_KITTEN type of card,
     * and in case it will be drawn, it will be verified whether the player has a DIFUSE card and can continue playing the
     * game or eliminate him otherwise.
     *
     *
     * @param player represents the player who is playing his turn
     */
    public void turnOfPlayer(Player player) {

        while (player.getNrOfTurns()>=1){

            boolean drawAndGo = false;
            boolean turning =false;
            boolean skip = false;

            while (!drawAndGo && !player.getDrawOfCards().isEmpty()) {

                for (int i = 0; i < player.getDrawOfCards().size(); i++) {
                    showThisToAll(i + ") " + player.getDrawOfCards().get(i).getTypes().toString());
                }

                showThisToAll("These are your cards, the order goes from 0 to the last card, and you must chose which one you want to put \n down. You end your turn by pressing -1 to draw a card " +
                        "\n If you want to play a combo you must press: -2 FOR DOUBLE COMBO \n -3 for TRIPLE COMBO and -5 for COMBO OF 5");
//                List<KitCard> allCardsInDeck = deck.seeAllCardsInDeck();
//                for (KitCard card : allCardsInDeck) {
//                    showThisToAll();(card);
//                }

                //int playerChoice = getPlayerChoice(scanner);

                int playerChoice = -1;
                showThisToAll(String.valueOf(player.getNrOfTurns()));
                showThisToAll(player.getPlayerName() + " now has " + player.getNrOfTurns() + " turns.");


                if (playerChoice == -2) {
                    showThisToAll("Player "+player.getPlayerName() + " wants to play a two cards combo");
                    showThisToAll("Will anybody contest this choice? aka NOPE him");
                    boolean nopeComboOfTwo = nopedd(player);
                    boolean isDoubleNoped = false;
                    boolean isThirdNoped = false;
                    boolean isFourthNoped = false;

                    if(nopeComboOfTwo) {
                        isDoubleNoped = doubleNoped();
                        if(isDoubleNoped){
                            isThirdNoped = thirdNope();
                            if(isThirdNoped){
                                isFourthNoped = fourthNope();
                            }
                        }
                    }
                    if(nopeComboOfTwo && !isDoubleNoped || isThirdNoped && !isFourthNoped){
                        showThisToAll("The combo was blocked! Sorry my friend!");
                    }
                    else
                    {
                        showThisToAll("Select the first card for the combo:");
                        int firstCard = getPlayerChoice(scanner);

                        showThisToAll("And also select the second card:");
                        int secondCard = getPlayerChoice(scanner);

                        if (secondComboValidity(firstCard, secondCard, player)) {

                            KitCard cardOne = player.getDrawOfCards().get(firstCard);
                            KitCard cardTwo = player.getDrawOfCards().get(secondCard);

                            if (cardOne.getTypes().equals(cardTwo.getTypes())) {
                                showThisToAll("Combo is possible");
                                favorRequest(player);


                                List<Integer> comboList = Arrays.asList(firstCard, secondCard);
                                Collections.sort(comboList, Collections.reverseOrder());
                                // de ce zice sa inlocuim ??
                                for (int cardToEliminate : comboList) {
                                    player.getDrawOfCards().remove(cardToEliminate);
                                }

                            }
                            else{
                                showThisToAll("Combo was not possible, please chose another option");
                            }
                        }
                    }

                }


                if (playerChoice == -3) {
                    showThisToAll("Thia player wants to play a 3 cards combo");
                    showThisToAll("Will anybody contest this choice ?");
                    boolean nopeComboOfThree = nopedd(player);
                    boolean isDoubleNoped = false;
                    boolean isThirdNoped = false;
                    boolean isFourthNoped = false;

                    if(nopeComboOfThree) {
                        isDoubleNoped = doubleNoped();
                        if(isDoubleNoped){
                            isThirdNoped = thirdNope();
                            if(isThirdNoped){
                                isFourthNoped = fourthNope();
                            }
                        }
                    }
                    if(nopeComboOfThree && !isDoubleNoped || isThirdNoped && !isFourthNoped){
                        showThisToAll("The combo was blocked! Sorry my friend!");
                    }
                    else{
                        if (player.getDrawOfCards().size() >= 3) {


                            showThisToAll("Select the first card for the combo: ");
                            int firstCard = getPlayerChoice(scanner);

                            showThisToAll("Select the second card: ");
                            int secondCard = getPlayerChoice(scanner);

                            showThisToAll("Select the third card: ");
                            int thirdCard = getPlayerChoice(scanner);

                            if (threeCardsComboValidity(firstCard, secondCard, thirdCard, player)) {

                                if (threeEqualCards(player.getDrawOfCards().get(firstCard), player.getDrawOfCards().get(secondCard), player.getDrawOfCards().get(thirdCard))){
                                    threeCardsCombo(player);
                                }
                                else
                                {
                                    showThisToAll("The cards are not of the same type.");
                                }
                            }
                            else
                            {
                                showThisToAll("Invalid card combo, you chose duplicate or out of bounds!");
                            }
                        }
                    }


                }


                if (playerChoice == -5) {
                    showThisToAll("Player "+ player.getPlayerName()+" wants no play a 5 cards combo");
                    showThisToAll("Will anybody contest?");


                    boolean isNoped = nopedd(player);
                    boolean isDoubleNoped = false;
                    boolean isThirdNoped = false;
                    boolean isFourthNoped = false;

                    if(isNoped) {
                        isDoubleNoped = doubleNoped();
                        if(isDoubleNoped){
                            isThirdNoped = thirdNope();
                            if(isThirdNoped){
                                isFourthNoped = fourthNope();
                            }
                        }
                    }

                    if(isNoped && !isDoubleNoped || isThirdNoped && !isFourthNoped)
                    {
                        showThisToAll("Sorry, no 5 cards combo for you, make another choice");
                        break;

                    }
                    else
                    {
                        if(!pile.isEmpty()){
                            if (player.getDrawOfCards().size() >= 5) {

                                showThisToAll("Select the first card for the combo: ");
                                int firstCard = getPlayerChoice(scanner);

                                showThisToAll("Select the second card: ");
                                int secondCard = getPlayerChoice(scanner);

                                showThisToAll("Select the third card: ");
                                int thirdCard = getPlayerChoice(scanner);

                                showThisToAll("Select the fourth card: ");
                                int fourthCard = getPlayerChoice(scanner);

                                showThisToAll("Select the fifth card: ");
                                int fifthCard = getPlayerChoice(scanner);

                                if (fiveCardsComboValidity(firstCard, secondCard, thirdCard, fourthCard, fifthCard, player)) {

                                    showThisToAll("Combo accepted. Choose from the following cards by selecting the index: ");
                                    for (int i = 0; i < getPile().size(); i++) {
                                        if (!getPile().get(i).getTypes().equals(KitCard.Type.EXPLODING_KITTEN)) {
                                            showThisToAll(i + " " + pile.get(i));
                                        }
                                    }

                                    boolean validChoice = false;

                                    while (!validChoice) {
                                        int choice = getPlayerChoice(scanner);
                                        if (choice >= 0 && choice < getPile().size()) {

                                            List<Integer> comboList = Arrays.asList(firstCard, secondCard, thirdCard, fourthCard, fifthCard);
                                            Collections.sort(comboList, Collections.reverseOrder());

                                            for (int cardToEliminate : comboList) {
                                                player.getDrawOfCards().remove(cardToEliminate);
                                            }

                                            player.getDrawOfCards().add(getPile().get(choice));
                                            getPile().remove(getPile().get(choice));

                                            drawAndGo = true;
                                            validChoice = true;
                                        } else {
                                            showThisToAll("Invalid index. Make another choice: ");
                                        }
                                    }

                                } else {
                                    showThisToAll("invalid combo");
                                    break;
                                }
                            } else {
                                showThisToAll("not enough cards for combo");
                                break;
                            }

                        }
                        else {
                            showThisToAll("Pile is empty. Combo not possible");
                            break;
                        }

                    }


                }

                if (playerChoice != -1 && playerChoice != -2 && playerChoice != -3 && playerChoice != -5) {

                    if (playerChoice >= 0 && playerChoice < player.drawOfCards.size()) {
                        KitCard usedCard = new KitCard(player.getDrawOfCards().get(playerChoice).getTypes());


                        switch (usedCard.getTypes()) {
                            //done
                            case SHUFFLE:
                                shuffleCard(player,usedCard,playerChoice);
                                break;

                            case NOPE, NORMAL_ONE, NORMAL_TWO, NORMAL_THREE, NORMAL_FOUR, NORMAL_FIVE:
                                player.drawOfCards.remove(playerChoice);
                                pile.add(usedCard);
                                break;

                            case SKIP:
                                showThisToAll("This player wants to skip");
                                showThisToAll("Will anybody contest this choice?");
                                boolean skipNope = nopedd(player);
                                boolean skipIsDoubleNoped = false;
                                boolean skipIsTripleNoped = false;
                                boolean skipIsFourthNoped = false;

                                if(skipNope){
                                    skipIsDoubleNoped = doubleNoped();
                                    if(skipIsDoubleNoped){
                                        skipIsTripleNoped = thirdNope();
                                        if(skipIsTripleNoped){
                                            skipIsFourthNoped = fourthNope();
                                        }
                                    }
                                }

                                if (skipNope && !skipIsDoubleNoped || skipIsTripleNoped && !skipIsFourthNoped)
                                {
                                    showThisToAll("The skip action was noped.");
                                    player.getDrawOfCards().remove(usedCard);
                                    pile.add(usedCard);
                                }
                                else
                                {
                                    player.drawOfCards.remove(playerChoice);
                                    pile.add(usedCard);
                                    skip = true;
                                    break;
                                }
                                break;

                            case FAVOR:
                                favorCard(player, usedCard, playerChoice);
                                break;

                            case SEE_THE_FUTURE:
                                seeTheFutureCard(player, usedCard, playerChoice);
                                break;

                            case ATTACK:

                                if(attackSomeone(player)){
                                    player.drawOfCards.remove(playerChoice);
                                    pile.add(usedCard);
                                    drawAndGo = true;
                                } else {
                                    player.drawOfCards.remove(playerChoice);
                                    pile.add(usedCard);
                                }

                                break;
                            default:
                                showThisToAll("Of viata");
                                break;
                        }
                    } else {
                        showThisToAll("card does not exist");
                    }
                }

                if (playerChoice == -1) {
                    if(player.getNrOfTurns()==1){
                        drawAndGo = true;
                        turning = false;
                        break;
                    }
                    else{
                        turning = true;
                        break;
                    }

                }
            }

            if (turning){
                showThisToAll("In turning");


                KitCard kitty = new KitCard(deck.seeABitOfTheFuture());
                if (kitty.getTypes().equals(KitCard.Type.EXPLODING_KITTEN)){
                    if (explodingOrNot(player)){
                        player.eliminate();
                        break;
                    }
                    else{
                        elimKittyFromPlayer(player);
                        player.decreaseTurns();
                    }
                }
                else if(skip){
                    player.decreaseTurns();
                }
                else{
                    player.drawCard(deck);
                    player.decreaseTurns();

                }

            }


            //draw new card and handle difuse Logic

            if (drawAndGo) {
                showThisToAll("In draw and go");

                KitCard ver = new KitCard(deck.seeABitOfTheFuture());





                if(ver.getTypes().equals(KitCard.Type.EXPLODING_KITTEN)){
                    if(explodingOrNot(player)){
                        player.eliminate();
                        break;
                    }
                    else{
                        player.drawCard(deck);
                        addExploding(player);
                        elimKittyFromPlayer(player);
                        player.decreaseTurns();
                        break;


                    }
                } else if (skip) {
                    player.decreaseTurns();
                    break;

                } else{
                    player.drawCard(deck);
                    player.decreaseTurns();

                    break;
                }

            }

        }

    }

    /**
     *Method called in the turnOfPlayer method which executes the action of a card of type SHUFFLE, depending on whether any
     * of the other players will NOPE or YUP the requested action.
     *
     * If the action is NOPEd, then the action of the card of type SHUFFLE will not take place, but in case of no NOPE, or
     * a YUP the action will stand and the cards in the deck will be shuffled.
     *
     * @param player represents the player who wishes to play the card of type SHUFFLE
     * @param usedCard represents the card that is being played
     * @param playerChoice the index of the card that is being played in the cards hand of the player
     */
    public void shuffleCard(Player player, KitCard usedCard, int playerChoice){
        showThisToAll("The player wants to shuffle");

        boolean isNoped = nopedd(player);
        boolean isDoubleNoped = false;
        boolean isThirdNoped = false;
        boolean isFourthNoped = false;

        if(isNoped) {
            isDoubleNoped = doubleNoped();
            if(isDoubleNoped){
                isThirdNoped = thirdNope();
                if(isThirdNoped){
                    isFourthNoped = fourthNope();
                }
            }
        }

        if(isNoped && !isDoubleNoped || isThirdNoped && !isFourthNoped) {
            showThisToAll("The shuffle action was noped.");

            player.drawOfCards.remove(playerChoice);
            pile.add(usedCard);
        } else {
            deck.shuffleAfterSetUp();
            player.drawOfCards.remove(playerChoice);
            pile.add(usedCard);
        }

    }

    /**
     *Method called in turnOfPlayer when the player wishes to play a card of Type SEE_THE_FUTURE.
     *
     * If the action is NOPEd, then the action of the card of type SHUFFLE will not take place, but in case of no NOPE, or
     * a YUP the action will stand and player who played the card will be able to see the first three cards n the deck.
     *
     * @param player represents the playes who wishes to play the card
     * @param usedCard the card that is being played
     * @param playerChoice represents the index of the card of the type SEE_THE_FUTURE in the hand of the player
     */
    public void seeTheFutureCard(Player player, KitCard usedCard, int playerChoice){
        showThisToAll("This player wants to see the future.");
        boolean isNopedforFuture = nopedd(player);
        boolean isDoubleNopedforFuture = false;
        boolean isTripleNopedFuture= false;
        boolean isFourthNopedFuture = false;

        if(isNopedforFuture){
            isDoubleNopedforFuture = doubleNoped();
            if(isDoubleNopedforFuture){
                isTripleNopedFuture = thirdNope();
                if(isTripleNopedFuture){
                    isFourthNopedFuture = fourthNope();
                }
            }
        }

        if(isNopedforFuture && !isDoubleNopedforFuture || isTripleNopedFuture && !isFourthNopedFuture) {
            showThisToAll("You may not see the future my friend:(.");
            player.drawOfCards.remove(playerChoice);
            pile.add(usedCard);
        } else {
            List<KitCard> oneTwoThreeKittens = deck.seeTheFuture();
            showThisToAll(String.valueOf(oneTwoThreeKittens));
            player.drawOfCards.remove(playerChoice);
            pile.add(usedCard);
        }

    }

    /**
     * Method that is being called in turnOfPlayer in case the player whose turn is wishes to play acard of type FAVOR.
     *
     * If the action is NOPEd, then the action of the card of type FAVOR will not take place, but in case of no NOPE, or
     * a YUP the action will stand and player who played the card will be able to ask an opponent player to give him a card.
     *
     * @param player represents the playes who wishes to play the card
     * @param usedCard the card that is being played
     * @param playerChoice represents the index of the card of the type FAVOR in the hand of the player
     */
    public void favorCard(Player player, KitCard usedCard, int playerChoice){
        showThisToAll("This player wants to ask for a favor");
        showThisToAll("Will anybody contest this choice?");
        boolean favorNope = nopedd(player);
        boolean favorIsDoubleNoped = false;
        boolean favorIsTripleNoped = false;
        boolean favorIsFourthNoped = false;

        if(favorNope){
            favorIsDoubleNoped = doubleNoped();
            if(favorIsDoubleNoped){
                favorIsTripleNoped = thirdNope();
                if(favorIsTripleNoped){
                    favorIsFourthNoped = fourthNope();
                }
            }
        }

        if (favorNope && !favorIsDoubleNoped || favorIsTripleNoped && !favorIsFourthNoped){
            showThisToAll("Action was noped, you may not ask for a favor");
            player.getDrawOfCards().remove(usedCard);
            pile.add(usedCard);

        }
        else{
            favorRequest(player);
            player.drawOfCards.remove(playerChoice);
            pile.add(usedCard);
        }
    }

    public void skipCard(Player player, KitCard usedCard, int playerChoice, boolean ceva){
        showThisToAll("This player wants to skip");
        showThisToAll("Will anybody contest this choice?");
        boolean skipNope = nopedd(player);
        boolean skipIsDoubleNoped = false;
        boolean skipIsTripleNoped = false;
        boolean skipIsFourthNoped = false;

        if(skipNope){
            skipIsDoubleNoped = doubleNoped();
            if(skipIsDoubleNoped){
                skipIsTripleNoped = thirdNope();
                if(skipIsTripleNoped){
                    skipIsFourthNoped = fourthNope();
                }
            }
        }

        if (skipNope && !skipIsDoubleNoped || skipIsTripleNoped && !skipIsFourthNoped)
        {
            showThisToAll("The skip action was noped.");
            player.getDrawOfCards().remove(usedCard);
            pile.add(usedCard);
        }
        else
        {
            player.drawOfCards.remove(playerChoice);
            pile.add(usedCard);
            ceva = true;
        }
    }

    /**
     * Method called when a player is in a possesion of a DIFUSE card and manages to stop himself from being eliminated
     * from the game.
     *
     * In this method, after the difuse action, he is asked on which position in the pile he wishes to
     * place the card of Type EXPLODING_KITTEN.
     *
     * @requires the player chooses a valid index on which the EXPLODING_KITTEN card will be placed
     * @param player represents the player who must place the EXPLODING_KITTEN card back in the deck
     */
    public void addExploding(Player player){

        showThisToAll("Successful DIFUSE! Now choose where you want to insert the Exploding Kitten card. \n To do that you have to pick a number from 0 (under all cards) to " + (deck.getCurrentNumberOfCards()) + "(on top of the cards) : \n");
        if(player instanceof ComputerPlayer){
            Random random = new Random();
            KitCard explodingKitty = new KitCard(KitCard.Type.EXPLODING_KITTEN);
            deck.insertExploding(explodingKitty, random.nextInt((deck.getCards().length - 1)));
        }
        else{
            while(true){
                //int playerInsert = getPlayerChoice(scanner);
                int playerInsert = 1;

                if (playerInsert >= 0 && playerInsert <= deck.getCards().length-1) {
                    KitCard explodingKitten = new KitCard(KitCard.Type.EXPLODING_KITTEN);
                    deck.insertExploding(explodingKitten, playerInsert);
                    showThisToAll("The player has inserted the kitten! ");
                    break;
                } else
                {
                    showThisToAll("Please choose from the possible options.");
                }
            }
        }

    }


    /**
     *Method which enables the start of a game of Exploding Kittens.
     *
     * This method adds back the required count of cards of type DIFUSE and EXPLODING_KITTEN, depending on the number
     * of players joining the game and starts the game by going through the list of players and making them take turns
     * until there is one player left.
     *
     * After the round is finished, the method calls the winner() method which displays the player who has won the round
     *
     * @requires at least two players in the ArrayList players for the start of a round
     */
    public void play(){

        deck.addCardsToTheDeck(KitCard.Type.EXPLODING_KITTEN, players.length-1);
        if (players.length<=4){
            deck.addCardsToTheDeck(KitCard.Type.DIFUSE, 2);
        }
        if(players.length == 5){
            deck.addCardsToTheDeck(KitCard.Type.DIFUSE, 1);
        }
        deck.shuffleAfterSetUp();

        showThisToAll("Welcome to our game of Exploding Kittens! Today we will have joining our game: ");
        for (Player player1 : players){
            player1.resetTurns();
            showThisToAll("Player "+ player1.getPlayerName());
            showThisToAll(" ");
        }

        while (!isGameOver()){
            for (Player player : players){
                if(!player.isEliminatedPlayer()){
                    showThisToAll("Player "+ player.getPlayerName()+ " it is your turn");
                    turnOfPlayer(player);
                    player.resetTurns();
                }
                player.resetTurns();

                if (isGameOver()) {
                    break;
                }
            }
        }
        winner();
    }

    //if the player draws an exploding kitten logic

    /**
     * Method called in turnOfPlayer, when the player who is currently playing will draw a card of type EXPLODING_KITTEN.
     *
     * This method goes to the players hand and searches for a card of type DIFUSE. If the DIFUSE is found, then the player
     * will be able to continue the game, and the card will be taken from his hand of cards, otherwise he will be eliminated.
     *
     * @param player the player who has drawn a card of type EXPLODING_KITTEN
     * @return true if the player is not in the possession of a card of type DIFUSE, false otherwise
     */
    private boolean explodingOrNot(Player player) {
        showThisToAll("EXPLODING KITTEN! Do you have a Diffuse card, or will you explode?");


        boolean foundDiffuse = false;
        Iterator<KitCard> iterator = player.getDrawOfCards().iterator();
        while (iterator.hasNext()) {
            KitCard card = iterator.next();
            if (card.getTypes().equals(KitCard.Type.DIFUSE)) {
                showThisToAll("Saved by the bell hihi");
                iterator.remove();
                foundDiffuse = true;
                break;
            }
        }
        if (foundDiffuse) {
//                    addExploding(player);
            return false;
        } else {
            showThisToAll("I am sorry, Difuse card could not be found");
            return true;
        }


    }


    /**
     * Method which acts as an intermediary step in the three cards combo in the turnOfPLayer, which has the function of
     * verifying whether the three cards chosen by the player for the combo are of the same type
     *
     * @param card1 represents the first card chosen for the combo
     * @param card2 represents the second card chosen for the combo
     * @param card3 represents the third card chosen for the combo
     * @return true if the cards are of the same type, false otherwise
     */
    public boolean threeEqualCards(KitCard card1, KitCard card2, KitCard card3){
        if (card1.getTypes().equals(card2.getTypes()) && card2.getTypes().equals(card3.getTypes())){
            return true;
        }
        return false;

    }

    /**
     * Method which has the function of iterating through the hand of cards of a player after he has difused a card of type
     * EXPLODING_KITTEN and eliminate it when and if found
     *
     * This method is used a assurance that the card of type EXPLODING_KITTEN will never be in the posession of any player
     *
     * @param player represents the player who has difused the EXPLODING_KITTEN card in the method turnOfPlayer
     */
    public void elimKittyFromPlayer(Player player){
        Iterator<KitCard> iterator = player.getDrawOfCards().iterator();
        while (iterator.hasNext()) {
            KitCard card = iterator.next();
            if (card.getTypes().equals(KitCard.Type.EXPLODING_KITTEN)) {
                iterator.remove();
                break;
            }
        }

    }


//nope logic

    /**
     * This method is called in the turnOfPlayer everytime the player who is currently playing wishes to play an action
     * card. If the opponents wish to NOPE the action that the player wants to play, this method will look through
     * the opponent hand of cards and each for a card of type NOPE. If that card is found, it is taken from the opponent's
     * hand and the action will be noped, case in which the other nope method will be called for the eventual stacking
     * of cards of type NOPE.
     *
     * @param player represents the player whose turn is
     * @return true if the NOPE action is successful, false otherwise
     */
    public boolean nopedd(Player player) {
        showThisToAll("Will anyone nope? 0 for yes, 1 for no");
        boolean noped = false;
        boolean yes = true;

        while (yes) {
            int choice = getPlayerChoice(scanner);
            if (choice == 0) {
                showThisToAll("Which one of you wants to nope " + player.getPlayerName() + "?");
                displayPlayers(player);

                boolean attack = true;

                while (attack) {
                    int attacker = getPlayerChoice(scanner);
                    if (isValidPlayer(attacker, player)) {
                        Iterator<KitCard> iterator = players[attacker].getDrawOfCards().iterator();
                        while (iterator.hasNext()) {
                            KitCard card = iterator.next();
                            if (card.getTypes().equals(KitCard.Type.NOPE)) {
                                showThisToAll("Valid NOPE");
                                iterator.remove();
                                pile.add(card);
                                noped = true;
                                break;
                            }
                        }
                        attack = false;
                    } else {
                        showThisToAll("Invalid choice");
                    }
                }
                yes = false;
            } else if (choice == 1) {
                yes = false;
            } else {
                showThisToAll("Invalid choice");
            }
        }
        return noped;
    }

    /**
     * Method called in turnOfPlayer when an action has been NOPED, which asks again, all of the players this time,
     * if they wish to contest the NOPE action and give the initial action of the player validity again by creating a YUP.
     *
     * @return true if any player successfully stacks another NOPE, false otherwise
     */
    public boolean doubleNoped() {
        boolean doubleNope = false;

        showThisToAll("NOPED OR YUPED HIHI?");
        showThisToAll("Does anybody want to contest this Nope?");

        while (true) {
            showThisToAll("0 for Yes, 1 for No");
            int choice = getPlayerChoice(scanner);

            if (choice == 0) {
                showThisToAll("Which one of you wants to contest this nopey nope ");
                displayAllPlayers();

                while (true) {
                    int attacker = getPlayerChoice(scanner);
                    if (validDoubleAttacker(attacker)) {
                        Iterator<KitCard> iterator = players[attacker].getDrawOfCards().iterator();
                        while (iterator.hasNext()) {
                            KitCard card = iterator.next();
                            if (card.getTypes().equals(KitCard.Type.NOPE)) {
                                showThisToAll("Valid DOUBLE NOPE, so a YUP YUP");
                                iterator.remove();
                                pile.add(card);
                                doubleNope = true;
                                return doubleNope;
                            }
                        }
                        showThisToAll("This player does not have a Nope card.");
                        return false;
                    } else {
                        showThisToAll("Invalid choice, choose a valid player.");
                    }
                }
            } else if (choice == 1) {
                return false;
            } else {
                showThisToAll("Invalid choice, choose 0 or 1.");
            }
        }
    }

    /**
     *Method called if the doubleNoped() method returns a true.
     * This method continues the stacking of cards of type NOPE, and ask the players if they wish to stack other card
     * and turn the action of the played card invalid again.
     *
     * @return true if any player successfully stacks another NOPE, false otherwise
     */
    public boolean thirdNope(){
        boolean thirdNoped = false;
        showThisToAll("The cards still stands");
        showThisToAll("Unless someone has another nopey nope");
        while (true) {
            showThisToAll("0 for Yes, 1 for No");
            int choice = getPlayerChoice(scanner);

            if (choice == 0) {
                showThisToAll("Which one of you wants to contest this YUP? ");
                displayAllPlayers();

                while (true) {
                    int attacker = getPlayerChoice(scanner);
                    if (validDoubleAttacker(attacker)) {
                        Iterator<KitCard> iterator = players[attacker].getDrawOfCards().iterator();
                        while (iterator.hasNext()) {
                            KitCard card = iterator.next();
                            if (card.getTypes().equals(KitCard.Type.NOPE)) {
                                showThisToAll("Valid Triple, so the nope stands");
                                iterator.remove();
                                pile.add(card);
                                thirdNoped = true;
                                return thirdNoped;
                            }
                        }
                        showThisToAll("This player does not have a Nope card.");
                        return false;
                    } else {
                        showThisToAll("Invalid choice, choose a valid player.");
                    }
                }
            } else if (choice == 1) {
                return false;
            } else {
                showThisToAll("Invalid choice, choose 0 or 1.");
            }
        }

    }

    /**
     *Method called if the tripleNope() method returns a true.
     * This method continues the stacking of cards of type NOPE, and ask the players if they wish to stack other card
     * and turn the action of the played card valid again.
     *
     * This method will be the last in the stacking process, as there are only four cards of type NOPE in a deck of
     * Exploding Kittens.
     *
     * @return true if any player successfully stacks another NOPE, false otherwise
     */
    public boolean fourthNope(){
        boolean fourthNope = false;
        showThisToAll("The cards still stands");
        showThisToAll("Unless someone has another nopey nope");
        showThisToAll("Fourth's a charm hihi");
        while (true) {
            showThisToAll("0 for Yes, 1 for No");
            int choice = getPlayerChoice(scanner);

            if (choice == 0) {
                showThisToAll("Which one of you wants to contest this triple NOPE? ");
                displayAllPlayers();

                while (true) {
                    int attacker = getPlayerChoice(scanner);
                    if (validDoubleAttacker(attacker)) {
                        Iterator<KitCard> iterator = players[attacker].getDrawOfCards().iterator();
                        while (iterator.hasNext()) {
                            KitCard card = iterator.next();
                            if (card.getTypes().equals(KitCard.Type.NOPE)) {
                                showThisToAll("Valid Fourth, so the card stays");
                                showThisToAll("YUP YUP");
                                iterator.remove();
                                pile.add(card);
                                fourthNope = true;
                                return fourthNope;
                            }
                        }
                        showThisToAll("This player does not have a Nope card.");
                        return false;
                    } else {
                        showThisToAll("Invalid choice, choose a valid player.");
                    }
                }
            } else if (choice == 1) {
                return false;
            } else {
                showThisToAll("Invalid choice, choose 0 or 1.");
            }
        }
    }


//display all players for other nopes

    /**
     * Method which has the function of displaying all the players and their player number (index)
     * This method is mainly used in the NOPE logic in order to see which of the players wishes to stack another
     * card of type NOPE.
     *
     * @requires players not null
     */
    public void displayAllPlayers() {
        int i = 0;
        for (Player player : players) {
            if (!player.isEliminatedPlayer()) {
                showThisToAll(i + " " + player.getPlayerName());
            }
            i++;
        }
    }


    /**
     *Method which has the function of displaying all the playersm, but the currentPlayer,  and their player number (index)
     * This method is mainly used in the NOPE logic in order to see which of the players wishes to stack another
     * card of type NOPE.
     *
     * @param currentPlayer represents the player who will be avoided from the listimg process
     */
    public void displayPlayers(Player currentPlayer) {
        int i = 0;
        for (Player player : players) {
            if (!player.equals(currentPlayer) && !player.isEliminatedPlayer()) {
                showThisToAll(i + ") " + player.getPlayerName());
            }
            i++;
        }
    }

    //player validation

    /**
     *Method used as a validation step in the noped() method in order to verify if the index of the playe who wishes
     * to NOPE is valid, but also verify if the index does not belong to the player whose turn is
     *
     * @param i represents the index of the player
     * @param player represents the player whose turn is in the method turnOfPlayer
     * @return true if the index is valid and does not belong to player, false otherwise
     */
    public boolean isValidPlayer(int i, Player player){
        if (i>=0 && i<players.length){
            if (!players[i].equals(player)){
                return true;
            }
        }
        return false;
    }

    /**
     * Method used to verify if a chosen index is valid and belongs to one of the players currenly playing in the game
     * @param i represents the index of the chosen player
     * @return true if the player index is in the index bounds and is not eliminated, false otherwise
     * @requires the player is not eliminated
     */
    public boolean validDoubleAttacker(int i){
        if (i >= 0 && i < players.length) {
            return !players[i].isEliminatedPlayer();
        }
        return false;

    }

    /**
     * Method used during the action of a card of type ATTACK. I first verifies if any player wants to NOPE the action,
     * double nope, triple nope, or fourth nope. If the NOPE is not played, or a YUP results from the stacking of nopes,
     * the next player after the turn of the player that is currently playing will have to take an extra turn
     *
     * @param player the player who wishes to play the ATTACK card
     * @return true if the attack is not noped, case in which the action will stand, false otherwise
     */
    public boolean attackSomeone(Player player) {
        int targetIndex = 0;
        for (int i = 0; i < players.length; i++) {
            if (players[i].equals(player)) {
                targetIndex = (i + 1) % players.length;
                break;
            }
        }
        showThisToAll("Player " + players[targetIndex].getPlayerName() + " you will be attacked");
        showThisToAll( "Will you nope? " );
        boolean nopeAttack = nopedd(player);
        boolean doubleNopeAttack = false;
        boolean tripleNopeAttack = false;
        boolean fourthNopeAttack = false;

        if(nopeAttack) {
            doubleNopeAttack = doubleNoped();
            if(doubleNopeAttack){
                tripleNopeAttack = thirdNope();
                if(tripleNopeAttack){
                    fourthNopeAttack = fourthNope();
                }
            }
        }
        if(nopeAttack && !doubleNopeAttack || tripleNopeAttack && !fourthNopeAttack){
            showThisToAll("This attack was noped N O P E D");
            return false;
        }
        else{
            showThisToAll("Sorry but the attack stands for the next player");
            if(player.getNrOfTurns() == 2){
                players[targetIndex].increaseTurns();
                players[targetIndex].increaseTurns();
            }
            players[targetIndex].increaseTurns();
            player.decreaseTurns();
            return true;
        }
    }

    public CardDeck getDeck() {
        return deck;
    }

    public List<Player> getPlayers() {
        return Arrays.asList(players);
    }
}
